const serve = require('serve'),
  Nightmare = require('nightmare'),
  findFreePort = require('find-free-port'),
  e2e_port_min = 4444,
  e2e_port_max = 8888;

let server;

const setupNightmareServer = (htdocsPath = 'htdocs/') => {
  const nightmare = Nightmare();

  beforeAll(done => {
    findFreePort(e2e_port_min, e2e_port_max, (err, freePort) => {
      if (err) {
        throw `Could not start server for E2E tests.\nNo free ports between ${e2e_port_min} and ${e2e_port_max}`;
      }

      server = serve(process.cwd(), {
        port: freePort,
        clipless: true
      });

      nightmare
        .wait(1000) // hacky hack, get rid of it: we wait for the server to be ready
        .goto(`http://localhost:${freePort}/test/e2e/${htdocsPath}`)
        .then(() => {
          done();
        }).catch(e => console.log(e));
    });
  });

  beforeEach(() => {
    nightmare
      .refresh()
      .wait(() => window.document.readyState === 'complete');
  });

  afterAll(() => {
    server.stop();
    nightmare.end()
  });

  return nightmare;
}

module.exports = {
  setupNightmareServer: setupNightmareServer
};
